angular
  .module('app')
  .controller('TimeslotController', ['$scope', '$state', 'Timeslot', 'Person', function($scope, $state, Timeslot, Person) {
    $scope.timeslots = [];
    $scope.newPerson = {};
    function getTimeslots() {
      Timeslot
        .getTimeSlots()
        .$promise
        .then(function(results) {
          $scope.timeslots = results.timeslots;
      });
    }
    getTimeslots();

    $scope.addPerson = function() {
      Person
        .create($scope.newPerson)
        .$promise
        .then(function() {
        $scope.newPerson = {};
        $scope.personForm.name.$setPristine();
        $scope.personForm.email.$setPristine();
        $('.focus').focus();
        getTimeslots();
      });
    };

    $scope.signUp= function(id) {
      alert(id);
    };

    $scope.localtime = function(time) {
      var localTime  = moment.utc(time).toDate();
      localTime = moment(localTime).format('ddd. MMM. Do h:mm A');
      return localTime;
    };
  }]);
