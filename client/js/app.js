angular
  .module('app', [
  'lbServices',
  'ui.router'
])
  .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('timeslot', {
      url: '',
      templateUrl: 'views/timeslot.html',
      controller: 'TimeslotController'
    });

    $urlRouterProvider.otherwise('timeslot');
  }]);
