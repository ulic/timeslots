module.exports = function(Timeslot) {
  Timeslot.getTimeSlots = function(cb) {
    Timeslot.find({order: 'time', include: {relation: 'people', scope: { fields: ['name'] } } }, function(err, response) {
      cb(null, response);
    });
  };

  Timeslot.remoteMethod (
    'getTimeSlots',
    {
      http: {path: '/getTimeSlots', verb: 'get'},
      returns: {arg: 'timeslots', type: 'string'}
    }
  );
};
